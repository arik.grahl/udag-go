package ddns

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/olekukonko/tablewriter"
	udagclient "gitlab.com/arik.grahl/udag-go/client"
)

const (
	defaultDNSResolveTimeout int    = 10
	defaultDNSResolveHost    string = "ns.udag.de"
	defaultDNSResolvePort    int    = 53
)

type Options struct {
	Proxy             string
	Email             string
	Password          string
	DNSResolveTimeout int
	DNSResolveHost    string
	DNSResolvePort    int
}

type DDNS struct {
	options *Options
	client  *udagclient.Client
}

type DDNSStatus struct {
	wanIP  *net.IP
	dnsIP  *net.IP
	status string
}

func NewDDNS(o *Options) (*DDNS, error) {
	if o == nil {
		o = &Options{}
	}

	if o.DNSResolveTimeout == 0 {
		o.DNSResolveTimeout = defaultDNSResolveTimeout
	}

	if o.DNSResolveHost == "" {
		o.DNSResolveHost = defaultDNSResolveHost
	}

	if o.DNSResolvePort == 0 {
		o.DNSResolvePort = defaultDNSResolvePort
	}

	c, err := udagclient.NewClient(&udagclient.Options{
		Proxy:    o.Proxy,
		Email:    o.Email,
		Password: o.Password,
	})
	if err != nil {
		return &DDNS{}, err
	}

	return &DDNS{options: o, client: c}, nil
}

func (d *DDNS) Check(domain string, subdomain string) error {
	status, err := d.getStatus(domain, subdomain)
	if err != nil {
		return err
	}

	table := newTable([]string{"Version", "WAN-IP", "DNS-IP", "Status"})
	for _, version := range []string{"v4", "v6"} {
		s, _ := status[version]

		wanIP := "–"
		if s.wanIP != nil {
			wanIP = s.wanIP.String()
		}

		dnsIP := "–"
		if s.dnsIP != nil {
			dnsIP = s.dnsIP.String()
		}

		table.Append([]string{version, wanIP, dnsIP, s.status})
	}

	table.Render()

	return nil
}

func (d *DDNS) getStatus(domain string, subdomain string) (map[string]DDNSStatus, error) {
	s := map[string]DDNSStatus{}

	wanIPs, err := d.getWANIPs()
	if err != nil {
		return s, nil
	}

	dnsIPs, err := d.getDNSIPs(fmt.Sprintf("%s.%s", subdomain, domain))
	if err != nil {
		return s, nil
	}

	for _, version := range []string{"v4", "v6"} {
		var wanIP *net.IP
		wanIPValue, present := wanIPs[version]
		if present {
			wanIP = &wanIPValue
		}

		var dnsIP *net.IP
		dnsIPValue, present := dnsIPs[version]
		if present {
			dnsIP = &dnsIPValue
		}

		status := ""
		if wanIP != nil && dnsIP != nil && wanIP.String() == dnsIP.String() {
			status = "up to date"
		} else if wanIP != nil && dnsIP != nil && wanIP.String() != dnsIP.String() {
			status = "out of sync"
		} else if dnsIP == nil {
			status = "ignoring (no DNS record)"
		} else if wanIP == nil {
			status = "ignoring (no WAN IP)"
		}

		s[version] = DDNSStatus{
			wanIP:  wanIP,
			dnsIP:  dnsIP,
			status: status,
		}
	}

	return s, nil
}

func (d *DDNS) Update(domain string, subdomain string) ([]udagclient.DomainRecord, error) {
	drs := []udagclient.DomainRecord{}

	status, err := d.getStatus(domain, subdomain)
	if err != nil {
		return drs, err
	}

	for version, s := range status {
		if s.status == "out of sync" {
			dr := udagclient.DomainRecord{
				Domain:    domain,
				Subdomain: subdomain,
				TTL:       300,
				Address:   s.wanIP.String(),
			}

			if version == "v6" {
				dr.Type = "AAAA"
			} else {
				dr.Type = "A"
			}

			dr, err := d.client.UpdateDomainRecord(dr)
			if err != nil {
				return drs, err
			}

			drs = append(drs, dr)
		}
	}

	return drs, err
}

func (d *DDNS) UpdateAndWaitForSynchronization(domain string, subdomain string) ([]udagclient.DomainRecord, error) {
	drs, err := d.Update(domain, subdomain)
	if err != nil {
		return drs, err
	}

	retries := 0

	if len(drs) > 0 {
		allSynchronized := false

		for retries <= 300 && !allSynchronized {
			status, err := d.getStatus(domain, subdomain)
			if err != nil {
				return drs, err
			}

			allSynchronized = true
			for _, s := range status {
				if s.status != "out of sync" {
					allSynchronized = false
				}
			}

			time.Sleep(1 * time.Second)
			retries += 1
		}
	}

	return drs, nil
}

func (d *DDNS) getWANIPs() (map[string]net.IP, error) {
	ips := map[string]net.IP{}

	res, err := http.Get("https://api.ipify.org")
	if err != nil {
		return ips, err
	}

	ipv4Bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return ips, err
	}

	ipv4 := net.ParseIP(string(ipv4Bytes))

	res, err = http.Get("https://api64.ipify.org")
	if err != nil {
		return ips, err
	}

	ipv4v6Bytes, err := io.ReadAll(res.Body)
	if err != nil {
		return ips, err
	}

	ipv4v6 := net.ParseIP(string(ipv4v6Bytes))

	if strings.Contains(ipv4.String(), ".") {
		ips["v4"] = ipv4
	}

	if strings.Contains(ipv4v6.String(), ":") {
		ips["v6"] = ipv4v6
	}

	return ips, nil
}

func (d *DDNS) getDNSIPs(domain string) (map[string]net.IP, error) {
	ips := map[string]net.IP{}

	r := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			dialer := net.Dialer{
				Timeout: time.Second * time.Duration(d.options.DNSResolveTimeout),
			}
			return dialer.DialContext(ctx, network, fmt.Sprintf("%s:%d", d.options.DNSResolveHost, d.options.DNSResolvePort))
		},
	}

	records, err := r.LookupHost(context.Background(), domain)

	if err != nil {
		return ips, err
	}

	reg := regexp.MustCompile("^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$")

	for _, record := range records {
		if reg.MatchString(record) {
			ips["v4"] = net.ParseIP(record)
		} else {
			ips["v6"] = net.ParseIP(record)
		}
	}

	return ips, nil
}

func newTable(header []string) *tablewriter.Table {
	var Out io.Writer = os.Stdout
	table := tablewriter.NewWriter(Out)
	table.SetHeader(header)
	table.SetAutoWrapText(false)
	table.SetAutoFormatHeaders(true)
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetCenterSeparator("")
	table.SetColumnSeparator("")
	table.SetRowSeparator("")
	table.SetHeaderLine(false)
	table.SetBorder(false)
	table.SetTablePadding("   ")
	table.SetNoWhiteSpace(true)

	return table
}
