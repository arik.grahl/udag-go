package cmd

import (
	"os"

	"github.com/spf13/cobra"
	udagproxy "gitlab.com/arik.grahl/udag-go/proxy"
)

var Address string

func init() {
	rootCmd.AddCommand(proxyCmd)
	proxyCmd.AddCommand(proxyStartCmd)

	proxyCmd.PersistentFlags().StringVarP(&Email, "email", "e", os.Getenv("UDAG_EMAIL"), "Email to authenticate with")
	proxyCmd.PersistentFlags().StringVarP(&Password, "password", "p", os.Getenv("UDAG_PASSWORD"), "Password to authenticate with")
	proxyStartCmd.PersistentFlags().StringVarP(&Address, "address", "a", ":8080", "Address to bind to")
}

var proxyCmd = &cobra.Command{
	Use:   "proxy",
	Args:  cobra.MatchAll(cobra.ExactArgs(0)),
	Short: "Proxy to United Domains which handles authentication",
	Long:  "Proxy to United Domains which handles authentication",
}

var proxyStartCmd = &cobra.Command{
	Use:   "start",
	Args:  cobra.MatchAll(cobra.ExactArgs(0)),
	Short: "Start proxy to United Domains which handles authentication",
	Long:  "Start proxy to United Domains which handles authentication",
	RunE:  proxyStart,
}

func proxyStart(cmd *cobra.Command, args []string) error {
	o := &udagproxy.Options{
		Email:    Email,
		Password: Password,
		Address:  Address,
	}

	proxy, err := udagproxy.NewProxy(o)
	if err != nil {
		return err
	}

	proxy.ListenAndServe()

	return nil
}
