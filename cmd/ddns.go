package cmd

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/spf13/cobra"
	udagddns "gitlab.com/arik.grahl/udag-go/ddns"
)

var Resolver string
var WaitForSynchronization bool

func init() {
	rootCmd.AddCommand(ddnsCmd)
	ddnsCmd.AddCommand(ddnsCheckCmd)
	ddnsCmd.AddCommand(ddnsUpdateCmd)

	ddnsCmd.PersistentFlags().StringVarP(&Email, "email", "e", os.Getenv("UDAG_EMAIL"), "Email to authenticate with")
	ddnsCmd.PersistentFlags().StringVarP(&Password, "password", "p", os.Getenv("UDAG_PASSWORD"), "Password to authenticate with")
	ddnsCmd.PersistentFlags().StringVarP(&Proxy, "proxy", "P", os.Getenv("UDAG_PROXY"), "Proxy to bypass authentication with")
	ddnsCmd.PersistentFlags().StringVarP(&Resolver, "resolver", "r", os.Getenv("UDAG_RESOLVER"), "DNS resolver host")
	ddnsUpdateCmd.PersistentFlags().BoolVarP(&WaitForSynchronization, "wait-for-synchronization", "w", os.Getenv("UDAG_WAIT_FOR_SYNCHRONIZATION") == "true", "Wait for DNS record to be synchronized")
}

var ddnsCmd = &cobra.Command{
	Use:   "ddns",
	Args:  cobra.MatchAll(cobra.ExactArgs(0)),
	Short: "A Dynamic DNS (DDNS) client",
	Long:  `A Dynamic DNS (DDNS) client to check and update DNS records managed by United Domains AG.`,
}

var ddnsCheckCmd = &cobra.Command{
	Use:   "check FQDN",
	Args:  cobra.MatchAll(cobra.ExactArgs(1)),
	Short: "Check a Dynamic DNS (DDNS) record",
	Long:  "Check a Dynamic DNS (DDNS) record",
	RunE:  ddnsCheck,
}

var ddnsUpdateCmd = &cobra.Command{
	Use:   "update FQDN",
	Args:  cobra.MatchAll(cobra.ExactArgs(1)),
	Short: "Update a Dynamic DNS (DDNS) record",
	Long:  "Update a Dynamic DNS (DDNS) record",
	RunE:  ddnsUpdate,
}

func parseFQDN(fqdn string) (string, string, error) {
	re := regexp.MustCompile(`^([a-z\-]+)\.([a-z\-]+)\.([a-z]+)$`)
	matches := re.FindStringSubmatch(fqdn)
	l := len(matches)
	if l != 4 {
		return "", "", errors.New(fmt.Sprintf("invalid FQDN %s", fqdn))
	}

	return strings.Join(matches[l-2:l], "."), matches[l-3], nil
}

func newDDNSClient() (*udagddns.DDNS, error) {
	o := &udagddns.Options{
		Email:          Email,
		Password:       Password,
		Proxy:          Proxy,
		DNSResolveHost: Resolver,
	}

	ddns, err := udagddns.NewDDNS(o)
	if err != nil {
		return &udagddns.DDNS{}, err
	}

	return ddns, nil
}

func ddnsCheck(cmd *cobra.Command, args []string) error {
	domain, subdomain, err := parseFQDN(args[0])
	if err != nil {
		return err
	}

	ddns, err := newDDNSClient()
	if err != nil {
		return err
	}

	err = ddns.Check(domain, subdomain)

	return err
}

func ddnsUpdate(cmd *cobra.Command, args []string) error {
	domain, subdomain, err := parseFQDN(args[0])
	if err != nil {
		return err
	}

	ddns, err := newDDNSClient()
	if err != nil {
		return err
	}

	if WaitForSynchronization {
		_, err = ddns.UpdateAndWaitForSynchronization(domain, subdomain)
	} else {
		_, err = ddns.Update(domain, subdomain)
	}

	return err
}
