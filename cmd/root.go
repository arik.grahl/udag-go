package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var Email string
var Password string
var Proxy string

var rootCmd = &cobra.Command{
	Use:   "udag",
	Short: "Udag is a CLI tool to manage DNS records managed by United Domains AG.",
	Long: `A CLI tool to manage DNS records managed by United Domains AG.
It is built around a client library which is intended to be used in arbitrary Go applications.
The CLI features a Dynamic DNS (DDNS) client, an API for the ExternalDNS Plugin Provider and an upstream proxy, which handles authentication.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
