package proxy

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	udagclient "gitlab.com/arik.grahl/udag-go/client"
)

const (
	udagBaseURL string = "https://www.united-domains.de"
)

type Options struct {
	Email    string
	Password string
	Address  string
}

type Proxy struct {
	options *Options
	client  *udagclient.Client
}

func NewProxy(o *Options) (*Proxy, error) {
	if o == nil {
		o = &Options{}
	}

	if o.Address == "" {
		o.Address = ":8080"
	}

	if o.Email == "" {
		o.Email = os.Getenv("UDAG_EMAIL")
	}

	if o.Password == "" {
		o.Password = os.Getenv("UDAG_PASSWORD")
	}

	c, err := udagclient.NewClient(&udagclient.Options{
		Email:    o.Email,
		Password: o.Password,
	})
	if err != nil {
		return &Proxy{}, err
	}

	return &Proxy{options: o, client: c}, nil
}

func (p *Proxy) ListenAndServe() {
	originServerURL, err := url.Parse(udagBaseURL)
	if err != nil {
		panic(err)
	}

	reverseProxy := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		req.Host = originServerURL.Host
		req.URL.Host = originServerURL.Host
		req.URL.Scheme = originServerURL.Scheme
		req.RequestURI = ""

		originServerResponse, err := p.client.DoAuthenticated(req)
		if err != nil {
			fmt.Println(err)
			rw.WriteHeader(http.StatusInternalServerError)
			_, _ = fmt.Fprint(rw, err)
			return
		}

		rw.WriteHeader(originServerResponse.StatusCode)
		io.Copy(rw, originServerResponse.Body)
	})

	http.ListenAndServe(p.options.Address, reverseProxy)
}
