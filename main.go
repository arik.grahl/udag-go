package main

import (
	"gitlab.com/arik.grahl/udag-go/cmd"
)

func main() {
	cmd.Execute()
}
