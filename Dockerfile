FROM docker.io/golang:1.19-alpine AS builder

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN CGO_ENABLED=0 go build -ldflags="-w -s" -o udag .

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/src/app/udag /udag

ENTRYPOINT ["/udag"]
