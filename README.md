# United Domains AG Golang

This project offers functionality around the domain services by United Domains AG.
It is an unofficial project for personal use-cases and is considered early alpha status software.

Essentially, this project contains a [client library](#client-library), which offers Go bindings to interact with DNS records managed by United Domains AG in a CRUD-like fashion.
Furthermore, this project features a CLI tool built around this library, which fulfills more abstract use cases.
These include a [Dynamic DNS (DDNS) Client](#dynamic-dns-ddns-client), an [API for ExternalDNS Plugin Provider](#api-for-externaldns-plugin-provider) and an [Upstream Proxy for Authentication](#upstream-proxy-for-authentication).

## Client Library

The client library offers Go bindings to interact with DNS records managed by United Domains AG in a CRUD-like fashion.
The following example gives and overview of its functionality.

```go
package main

import (
	"fmt"

	udagclient "gitlab.com/arik.grahl/udag-go/client"
)

func main() {
	o := &udagclient.Options{
		// email to authenticate with (defaults to environment variable $UDAG_EMAIL)
		Email:    "mail@example.com",     

		// password to authenticate with (defaults to environment variable $UDAG_PASSWORD)
		Password: "12345678",

		// optional proxy to use to bypass authentication
		Proxy:    "http://localhost:8080",
	}

	udag, err := udagclient.NewClient(o)
	if err != nil {
		panic(err)
	}

	// get all domains for the associated account
	fmt.Println(udag.Domains())

	// get all subdomains for the given domain
	fmt.Println(udag.Subdomains("example.com")) // by domain name
	fmt.Println(udag.Subdomains(1234567))       // by id

	// get all domain records for the given domain
	fmt.Println(udag.DomainRecords("example.com")) // by domain name
	fmt.Println(udag.DomainRecords(1234567))       // by id

	// get DNS zone file for the given domain
	fmt.Println(udag.ZoneFile("example.com")) // by domain name
	fmt.Println(udag.ZoneFile(1234567))       // by id

	// create a CNAME record for the FQDN foo.example.com with the target bar.example.com
	dr := udagclient.DomainRecord{
		Domain:    "example.com",
		Subdomain: "foo",
		TTL:       600,
		Target:    "bar.example.com",
		Type:      "CNAME",
	}
	fmt.Println(udag.CreateDomainRecord(dr))

	// update a CNAME record for the FQDN foo.example.com to the target foobar.example.com
	dr = udagclient.DomainRecord{
		Domain:    "example.com",
		Subdomain: "foo",
		TTL:       600,
		Target:    "foobar.example.com",
		Type:      "CNAME",
	}
	fmt.Println(udag.UpdateDomainRecord(dr))

	// remove the CNAME record for the FQDN foo.example.com
	dr = udagclient.DomainRecord{
		Domain:    "example.com",
		Subdomain: "test",
		Type:      "CNAME",
	}
	fmt.Println(udag.RemoveDomainRecord(dr))
}
```

## Dynamic DNS (DDNS) Client

The Dynamic DNS (DDNS) client offers checking and updating DNS records managed by United Domains AG.

### `check`

```
Usage:
  udag ddns check FQDN [flags]

Flags:
  -h, --help   help for check

Global Flags:
  -e, --email string      Email to authenticate with
  -p, --password string   Password to authenticate with
  -P, --proxy string      Proxy to bypass authentication with
  -r, --resolver string   DNS resolver host
```

Example output:

```
VERSION   WAN-IP                               DNS-IP           STATUS
v4        93.184.216.34                        1.2.3.4          out of sync
v6        2606:2800:220:1:248:1893:25c8:1946   –                ignoring (no DNS record)
```

### `update`

```
Usage:
  udag ddns update FQDN [flags]

Flags:
  -h, --help                       help for update
  -w, --wait-for-synchronization   Wait for DNS record to be synchronized

Global Flags:
  -e, --email string      Email to authenticate with
  -p, --password string   Password to authenticate with
  -P, --proxy string      Proxy to bypass authentication with
  -r, --resolver string   DNS resolver host
```

## API for ExternalDNS Plugin Provider

This API is currently under active development and relies on ExternalDNS' [Plugin Provider](https://github.com/kubernetes-sigs/external-dns/pull/3063), which is at the moment of writing not yet released.
This component implements the Plugin Provider's HTTP ReST-API in order to fulfill domain change plans computed by ExternalDNS.

## Upstream Proxy for Authentication

Since authentication is limited to a certain amount per time window and IP address, there may be the use case for maintaining a long running session, which clients can use.
Such a long running session can be offered by this Proxy, which clients can address in order to bypass authentication completely.
This proxy is particularly interesting for local development.
For security reasons this proxy should always be bound to localhost as it offers no additional authentication and giving full access to the complete domain portfolio.

```
Usage:
  udag proxy start [flags]

Flags:
  -a, --address string   Address to bind to (default ":8080")
  -h, --help             help for start

Global Flags:
  -e, --email string      Email to authenticate with
  -p, --password string   Password to authenticate with
```
