package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/publicsuffix"
)

const (
	userAgent   string = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0"
	udagBaseURL string = "https://www.united-domains.de"
)

type Options struct {
	Proxy    string
	Email    string
	Password string
	Retries  int
}

type Client struct {
	httpClient *http.Client
	csrf       string
	csrfMeta   string
	options    *Options
}

type Domain struct {
	ID              int             `json:"id"`
	Domain          string          `json:"domain"`
	Configurable    bool            `json:"configurable"`
	DomainLockState DomainLockState `json:"domain_lock_state"`
}

type DomainLockState struct {
	DomainLocked bool `json:"domain_locked"`
	EmailLocked  bool `json:"email_locked"`
	WebLocked    bool `json:"web_locked"`
}

type DomainRecord struct {
	Address       string `json:"address,omitempty"`
	Domain        string `json:"domain"`
	FilterValue   string `json:"filter_value"`
	FormID        string `json:"formId,omitempty"`
	ID            *int   `json:"id"`
	StandardValue bool   `json:"standard_value"`
	Subdomain     string `json:"sub_domain"`
	TTL           int    `json:"ttl"`
	Target        string `json:"target,omitempty"`
	Type          string `json:"type"`
}

func NewClient(o *Options) (*Client, error) {
	if o == nil {
		o = &Options{}
	}

	if o.Proxy == "" {
		o.Proxy = udagBaseURL
	}

	if o.Email == "" {
		o.Email = os.Getenv("UDAG_EMAIL")
	}

	if o.Password == "" {
		o.Password = os.Getenv("UDAG_PASSWORD")
	}

	if o.Retries == 0 {
		o.Retries = 3
	}

	client, err := newHTTPClient()
	if err != nil {
		return &Client{}, err
	}

	udagClient := &Client{httpClient: client, options: o}

	return udagClient, nil
}

func newHTTPClient() (*http.Client, error) {
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return &http.Client{}, err
	}

	client := &http.Client{
		Jar: jar,
	}

	return client, nil
}

func (c *Client) login() error {
	c.csrf = ""
	c.csrfMeta = ""

	resp, err := c.httpClient.Get(udagBaseURL + "/login/")
	if err != nil {
		return err
	}

	csrf, csrfMeta, err := extractCSRF(resp.Body)
	if err != nil {
		return err
	}

	c.csrf = csrf
	c.csrfMeta = csrfMeta

	data := url.Values{}
	data.Set("language", "de")

	req, err := http.NewRequest("POST", udagBaseURL+"/set-user-language", strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("HTTP-X-CSRF-TOKEN", c.csrfMeta)

	resp, err = c.httpClient.Do(req)
	if err != nil {
		return err
	}

	data = url.Values{}
	data.Set("csrf", c.csrf)
	data.Set("selector", "login")
	data.Set("email", c.options.Email)
	data.Set("pwd", c.options.Password)
	data.Set("loginBtn", "Login")

	req, err = http.NewRequest("POST", udagBaseURL+"/login/", strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err = c.httpClient.Do(req)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) DoAuthenticated(req *http.Request) (*http.Response, error) {
	httpStatus := 600
	attempt := 1

	for httpStatus >= 400 && attempt <= c.options.Retries {
		req.Header.Set("User-Agent", userAgent)

		if c.csrfMeta != "" && req.Method != http.MethodGet {
			fmt.Printf("set CSRF token %s\n", c.csrfMeta)
			req.Header.Set("Http-X-Csrf-Token", c.csrfMeta)
		}

		resp, err := c.httpClient.Do(req)
		if err != nil {
			return nil, err
		}

		httpStatus = resp.StatusCode

		fmt.Println(attempt, httpStatus, req.Method, req.URL)

		if c.options.Proxy == udagBaseURL && (httpStatus == 401 || httpStatus == 422) {
			fmt.Println("perform login")

			httpClient, err := newHTTPClient()
			if err != nil {
				return nil, err
			}

			c.httpClient = httpClient
			c.csrf = ""
			c.csrfMeta = ""
			req.Header.Del("Http-X-Csrf-Token")

			err = c.login()
			if err != nil {
				return nil, err
			}

			newReq, err := http.NewRequest(req.Method, req.URL.String(), req.Body)
			if err != nil {
				return nil, err
			}

			newReq.Header = req.Header
			req = newReq
		} else {
			return resp, nil
		}

		attempt += 1
	}

	return nil, errors.New("Request retries unsuccessfully exhausted")
}

func (c *Client) authenticatedRequest(method string, url string, headers map[string]string, data io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, data)
	if err != nil {
		return nil, err
	}

	for key, value := range headers {
		req.Header.Add(key, value)
	}

	resp, err := c.DoAuthenticated(req)

	return resp, err
}

func (c *Client) Domains() ([]Domain, error) {
	resp, err := c.authenticatedRequest("GET", fmt.Sprintf("%s/pfapi/dns/domain-list", c.options.Proxy), map[string]string{}, strings.NewReader(""))

	if err != nil {
		return []Domain{}, err
	}

	var decodedResponse map[string][]Domain
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		return []Domain{}, err
	}

	return decodedResponse["data"], nil
}

func (c *Client) Subdomains(domain interface{}) ([]string, error) {
	domainID, err := c.getDomainID(domain)
	if err != nil {
		return []string{}, err
	}

	resp, err := c.authenticatedRequest("GET", fmt.Sprintf("%s/pfapi/dns/domain/%d/subdomain-list", c.options.Proxy, domainID), map[string]string{}, strings.NewReader(""))
	if err != nil {
		return []string{}, err
	}

	var decodedResponse map[string][]string
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		return []string{}, err
	}

	return decodedResponse["data"], nil
}

func (c *Client) DomainRecords(domain interface{}) (map[string][]DomainRecord, error) {
	domainID, err := c.getDomainID(domain)
	if err != nil {
		return map[string][]DomainRecord{}, err
	}

	resp, err := c.authenticatedRequest("GET", fmt.Sprintf("%s/pfapi/dns/domain/%d/records", c.options.Proxy, domainID), map[string]string{}, strings.NewReader(""))
	if err != nil {
		return map[string][]DomainRecord{}, err
	}

	var decodedResponse map[string]map[string][]DomainRecord
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		return map[string][]DomainRecord{}, err
	}

	return decodedResponse["data"], nil
}

func (c *Client) ZoneFile(domain interface{}) (string, error) {
	domainID, err := c.getDomainID(domain)
	if err != nil {
		return "", err
	}

	resp, err := c.authenticatedRequest("GET", fmt.Sprintf("%s/pfapi/dns/domain/%d/export", c.options.Proxy, domainID), map[string]string{}, strings.NewReader(""))
	if err != nil {
		return "", err
	}

	zoneFile, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(zoneFile), nil
}

func (c *Client) CreateDomainRecord(dr DomainRecord) (DomainRecord, error) {
	domainID, err := c.getDomainID(dr.Domain)
	if err != nil {
		return DomainRecord{}, err
	}

	data := map[string]interface{}{
		"domain_lock_state": DomainLockState{
			DomainLocked: false,
			EmailLocked:  false,
			WebLocked:    false,
		},
		"record": dr,
	}

	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return DomainRecord{}, err
	}

	headers := map[string]string{
		"Content-Type": "application/json; charset=utf-8",
	}

	resp, err := c.authenticatedRequest("PUT", fmt.Sprintf("%s/pfapi/dns/domain/%d/records", c.options.Proxy, domainID), headers, bytes.NewReader(jsonBytes))
	if err != nil {
		return DomainRecord{}, err
	}

	var decodedResponse map[string]DomainRecord
	err = json.NewDecoder(resp.Body).Decode(&decodedResponse)
	if err != nil {
		return DomainRecord{}, err
	}

	return decodedResponse["data"], nil
}

func (c *Client) UpdateDomainRecord(dr DomainRecord) (DomainRecord, error) {
	domainID, err := c.getDomainID(dr.Domain)
	if err != nil {
		return DomainRecord{}, err
	}

	search, err := c.getDomainRecord(domainID, dr)
	if err != nil {
		return DomainRecord{}, err
	}

	dr.ID = search.ID

	data := map[string]interface{}{
		"domain_lock_state": DomainLockState{
			DomainLocked: false,
			EmailLocked:  false,
			WebLocked:    false,
		},
		"record": dr,
	}

	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return DomainRecord{}, err
	}

	headers := map[string]string{
		"Content-Type": "application/json; charset=utf-8",
	}

	_, err = c.authenticatedRequest("PUT", fmt.Sprintf("%s/pfapi/dns/domain/%d/records", c.options.Proxy, domainID), headers, bytes.NewReader(jsonBytes))
	if err != nil {
		return DomainRecord{}, err
	}

	return dr, nil
}

func (c *Client) RemoveDomainRecord(dr DomainRecord) (DomainRecord, error) {
	domainID, err := c.getDomainID(dr.Domain)
	if err != nil {
		return DomainRecord{}, err
	}

	search, err := c.getDomainRecord(domainID, dr)
	if err != nil {
		return DomainRecord{}, err
	}

	dr.ID = search.ID

	data := map[string]interface{}{
		"domain_lock_state": DomainLockState{
			DomainLocked: false,
			EmailLocked:  false,
			WebLocked:    false,
		},
		"record": dr,
	}

	json, err := json.Marshal(data)
	if err != nil {
		return DomainRecord{}, err
	}

	headers := map[string]string{
		"Content-Type": "application/json; charset=utf-8",
	}

	_, err = c.authenticatedRequest("PUT", fmt.Sprintf("%s/pfapi/dns/domain/%d/records/remove", c.options.Proxy, domainID), headers, bytes.NewReader(json))
	if err != nil {
		return DomainRecord{}, err
	}

	return dr, nil
}

func (c *Client) getDomainID(search interface{}) (int, error) {
	switch search.(type) {
	case string:
		domains, err := c.Domains()
		if err != nil {
			return 0, err
		}

		for _, domain := range domains {
			if domain.Domain == search.(string) {
				return domain.ID, nil
			}
		}

		return 0, errors.New(fmt.Sprintf("Domain %s not found", search))
	case int:
		return search.(int), nil
	}

	return 0, nil
}

func (c *Client) getDomainRecord(domainID int, search DomainRecord) (DomainRecord, error) {
	drs, err := c.DomainRecords(search.Domain)
	if err != nil {
		return DomainRecord{}, err
	}

	for _, dr := range drs[search.Type] {
		if dr.Subdomain == search.Subdomain {
			return dr, nil
		}
	}

	return DomainRecord{}, errors.New(fmt.Sprintf("DomainRecord of type %s for Subdomain %s and DomainID %d not found", search.Type, search.Subdomain, domainID))
}

func extractCSRF(input io.Reader) (string, string, error) {
	doc, err := goquery.NewDocumentFromReader(input)

	if err != nil {
		return "", "", err
	}

	csrf := ""

	doc.Find("#order-login-form input[name=\"csrf\"]").Each(func(i int, s *goquery.Selection) {
		csrf, _ = s.Attr("value")
	})

	csrfMeta := ""

	doc.Find("meta[name=\"csrf\"]").Each(func(i int, s *goquery.Selection) {
		csrfMeta, _ = s.Attr("content")
	})

	return csrf, csrfMeta, nil
}
